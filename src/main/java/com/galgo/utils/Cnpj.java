package com.galgo.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Cnpj {
	
	private static final Logger log = LoggerFactory.getLogger(Cnpj.class);
	
	/**
	 * Codigo obtido em <a href="http://www.guj.com.br/java/227474-gerar-cnpj">
	 * http://www.guj.com.br/java/227474-gerar-cnpj</a>
	 * 
	 * @param str_cnpj
	 * @return
	 */
	public static boolean validar(String str_cnpj) {

		if (!str_cnpj.substring(0, 1).equals("")) {

			try {
				str_cnpj = str_cnpj.replace('.', ' ');
				str_cnpj = str_cnpj.replace('/', ' ');
				str_cnpj = str_cnpj.replace('-', ' ');
				str_cnpj = str_cnpj.replaceAll(" ", "");
				int soma = 0, dig;
				String cnpj_calc = str_cnpj.substring(0, 12);
				
				if (str_cnpj.length() != 14)
					return false;
				char[] chr_cnpj = cnpj_calc.toCharArray();

				/* Primeira parte */
				for (int i = 0; i < 4; i++)
					if (chr_cnpj[i] - 48 >= 0 && chr_cnpj[i] - 48 <= 9)
						soma += (chr_cnpj[i] - 48) * (6 - (i + 1));
				for (int i = 0; i < 8; i++)
					if (chr_cnpj[i + 4] - 48 >= 0 && chr_cnpj[i + 4] - 48 <= 9)
						soma += (chr_cnpj[i + 4] - 48) * (10 - (i + 1));
				dig = 11 - (soma % 11);
				cnpj_calc += (dig == 10 || dig == 11) ?
						"0" : Integer.toString(dig);
				
				/* Segunda parte */
				soma = 0;
				chr_cnpj = cnpj_calc.toCharArray();
				for (int i = 0; i < 5; i++)
					if (chr_cnpj[i] - 48 >= 0 && chr_cnpj[i] - 48 <= 9)
						soma += (chr_cnpj[i] - 48) * (7 - (i + 1));
				for (int i = 0; i < 8; i++)
					if (chr_cnpj[i + 5] - 48 >= 0 && chr_cnpj[i + 5] - 48 <= 9)
						soma += (chr_cnpj[i + 5] - 48) * (10 - (i + 1));
				dig = 11 - (soma % 11);
				cnpj_calc += (dig == 10 || dig == 11) ?
						"0" : Integer.toString(dig);
				return str_cnpj.equals(cnpj_calc);
			} catch (Exception e) {
				log.error("Erro !" + e);
				return false;
			}
		} else
			return false;

	}
	
	public static String gerarCnpj() {
		// Gerando aleatoriamente uma sequencia de 12 digitos
		String str_cnpj = "";     
        Integer numero;     
        for (int i = 0; i < 12; i++) {     
            numero = new Integer((int) (Math.random() * 10));     
            str_cnpj += numero.toString();
        }     
		
		// 
        String cnpj_calc = str_cnpj.substring(0, 12);
		char[] chr_cnpj = cnpj_calc.toCharArray();
		int soma = 0, dig;
		
		/* Primeira parte */
		for (int i = 0; i < 4; i++)
			if (chr_cnpj[i] - 48 >= 0 && chr_cnpj[i] - 48 <= 9)
				soma += (chr_cnpj[i] - 48) * (6 - (i + 1));
		for (int i = 0; i < 8; i++)
			if (chr_cnpj[i + 4] - 48 >= 0 && chr_cnpj[i + 4] - 48 <= 9)
				soma += (chr_cnpj[i + 4] - 48) * (10 - (i + 1));
		dig = 11 - (soma % 11);
		cnpj_calc += (dig == 10 || dig == 11) ?
				"0" : Integer.toString(dig);
		
		/* Segunda parte */
		soma = 0;
		chr_cnpj = cnpj_calc.toCharArray();
		for (int i = 0; i < 5; i++)
			if (chr_cnpj[i] - 48 >= 0 && chr_cnpj[i] - 48 <= 9)
				soma += (chr_cnpj[i] - 48) * (7 - (i + 1));
		for (int i = 0; i < 8; i++)
			if (chr_cnpj[i + 5] - 48 >= 0 && chr_cnpj[i + 5] - 48 <= 9)
				soma += (chr_cnpj[i + 5] - 48) * (10 - (i + 1));
		dig = 11 - (soma % 11);
		cnpj_calc += (dig == 10 || dig == 11) ?
				"0" : Integer.toString(dig);
		return cnpj_calc;
		
	}

	public static void main(String[] args) {
		String cnpj = gerarCnpj();
		log.info(gerarCnpj() + ": " + validar(cnpj));
	}
}
