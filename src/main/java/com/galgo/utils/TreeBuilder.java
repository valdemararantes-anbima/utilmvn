package com.galgo.utils;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.annotation.XmlType;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author valdemar.arantes
 */
public class TreeBuilder {

    private static final Logger log = LoggerFactory.getLogger(TreeBuilder.class);
    private final Class<?> beanClass;
    private final String beanPropName;
    private Tree<Node> t;

    public TreeBuilder(Class<?> beanClass, String beanName) {
        this.beanClass = beanClass;
        this.beanPropName = beanName;
    }

    public static List<Node> getChildren(Class<?> propClass) {
        XmlType xmlType = propClass.getAnnotation(XmlType.class);
        if (xmlType == null) {
            return null;
        }

        String[] propOrder = xmlType.propOrder();
        if (propOrder == null || propOrder.length == 0 || (
                propOrder.length == 1 && StringUtils.
                        isBlank(propOrder[0])
        )) {
            log.info("propOrder da anotação XmlType da classe {} não está definido");
            return null;
        }

        // Criando um mapa propName X propType da classe propClass
        PropertyDescriptor[] descrList = PropertyUtils.getPropertyDescriptors(propClass);
        //Map<String, Class<?>> propName2propType = new HashMap<>();
        List<Node> nodeList = new ArrayList<>(propOrder.length);
        for (String propName : propOrder) {
            try {
                //propName2propType.put(propName, propClass.getDeclaredField(propName).getType());
                nodeList.add(new Node(propName, propClass.getDeclaredField(propName).getType()));
            } catch (NoSuchFieldException e) {
                log.error(null, e);
            }
        }

        //        for (String prop : propOrder) {
        //            nodeList.add(new Node(prop, propName2propType.get(prop)));
        //        }

        return nodeList;
    }

    public Tree<Node> addNode(Tree<Node> t1, Node node) {
        Tree<Node> ret;
        log.trace("node={}", node);
        if (t1 == null) {
            ret = new Tree<>(node);
            t = ret;
        } else {
            ret = t1.addLeaf(node);
        }

        if (t1 != null) {
            Field f = null;
            Class<?> beanClass = t1.getHead().propClass;
            if (ClassUtils.isListField(beanClass, node.propName)) {
                Class<?> aClass = ClassUtils.getGenericTypeFromField(beanClass, node.propName);
                node.isList = true;
                if (aClass != null) {
                    node.propClass = aClass;

                } else {
                    log.warn("ClassUtils.getGenericTypeFromField null para beanClass={} e propName={}", beanClass,
                            node.propName);
                }
            }


            //            try {
            //                f = t1.getHead().propClass.getDeclaredField(node.propName);
            //            } catch (Exception ex) {
            //                ex.printStackTrace();
            //            }
            //            Type tp = f.getGenericType();
            //            if (tp instanceof ParameterizedType) {
            //                log.debug("{} é um List", node.propName);
            //                ParameterizedType pt = (ParameterizedType) tp;
            //                Type[] actualTypes = pt.getActualTypeArguments();
            //                Class<?> aClass = (Class<?>)actualTypes[0];
            //                node.isList = true;
            //                node.propClass = aClass;
            //            }
        }

        if (node.isList) {
            int i = node.getClass().getTypeParameters().length;
        }

        List<Node> children = getChildren(node.propClass);

        if (CollectionUtils.isEmpty(children)) {
            return ret;
        }

        for (Node child : children) {
            addNode(ret, child);
        }

        return ret;
    }

    public Tree<Node> buildTree() {
        log.info("Construindo a árvores de classes...");
        XmlType xmlType = beanClass.getAnnotation(XmlType.class);
        Node root = new Node(beanPropName, beanClass);
        addNode(null, root);
        log.debug(t.toString());
        return t;
    }

    /**
     * @param n
     * @return Uma string com o nome aninhado da propriedade
     */
    public String getNestePropertyName(final Node n) {
        String ret = "";
        Node actualNode = n;
        do {
            ret = actualNode.propName + (actualNode.isList ? "[]" : "") + "." + ret;
            Tree<Node> parent = t.getTree(actualNode).getParent();
            if (parent == null) {
                break;
            }
            actualNode = parent.getHead();
        } while (true);

        // Removendo o "." que fica no fim da String por conta do algoritmo acima
        return ret.substring(0, ret.length() - 1);
    }

    /**
     * @param subTree
     * @return Uma string com o nome aninhado da propriedade associada à raiz da árvore subTree
     */
    public String getNestedPropertyName(Tree<Node> subTree) {
        String ret = "";

        if (subTree == null) {
            return null;
        }

        if (subTree.getParent() == null) {
            // Estamos na raiz da árvore à qual esta subTree deveria pertencer
            if (subTree != t) {
                // Inválida! Esta sub árvore não pertence à árvore t desta TreeBuilder
                throw new ApplicationException(String.format("Sub-árvore %1s não pertence à árvore %2s", subTree, t));
            }

            return subTree.getHead().propName;
        }

        Node stHead = subTree.getHead();
        ret = getNestedPropertyName(subTree.getParent()) + "." + stHead.propName + (stHead.isList ? "[]" : "");
        return ret;
    }

    public static class Node {

        public String propName;
        public Class<?> propClass;
        public boolean isList;

        public Node(String propName, Class<?> propClass) {
            this.propName = propName;
            this.propClass = propClass;
        }

        @Override
        public String toString() {
            return String.format("%1s%2s | %3s", propName, isList ? "[]" : "",
                    propClass == null ? "null" : propClass.getName());
        }
    }
}
