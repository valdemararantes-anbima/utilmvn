package com.galgo.utils;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.beanutils.BeanUtils;

public class BeansListUtils {
	public static String toString(List<?> beansList, String property) {
		List<String> strs = new ArrayList<String>(beansList.size());
		for (Object bean : beansList) {
			try {
				strs.add(BeanUtils.getProperty(bean, property));
			} catch (Exception e) {
				e.printStackTrace();
				strs.add("ERROR: " + e.getMessage());
			}
		}
		return strs.toString();
	}
}
