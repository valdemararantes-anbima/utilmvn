/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galgo.utils.cert_digital;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.X509Certificate;
import javax.net.ssl.HttpsURLConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
public class TestHttpsURLConnection {

    private static final Logger log = LoggerFactory.getLogger(TestHttpsURLConnection.class);
    public static final String https_url = "https://ws.homologacao.sistemagalgo:7843/ServicePLCota";

    public static void main(String args[]) {
        TrustAllCerts.doIt();
        testHttpsURLConnection();
        testURLConnection();
    }

    public static void testHttpsURLConnection() {
        try {
            URL url = new URL(https_url);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            log.debug("Default SocketFactory={}", HttpsURLConnection.getDefaultSSLSocketFactory());

            //dumpl all cert info
            print_https_cert(con);

            //dump all the content
            // print_content(con);

        } catch (Exception e) {
            log.error(null, e);
        }
    }

    public static void testURLConnection() {
        try {
            URL url = new URL(https_url);
            log.debug("Conectando...");
            log.debug("Default SocketFactory={}", HttpsURLConnection.getDefaultSSLSocketFactory());
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            log.debug("Obtendo o Response Code");
            log.debug("Response Message = {}", con.getResponseMessage());
        } catch (Exception e) {
            log.error(null, e);
        }
    }

    private static void print_https_cert(HttpsURLConnection con) {

        if (con != null) {

            try {
                StringBuilder buff = new StringBuilder();
                buff.append("Response Code : ").append(con.getResponseCode()).
                append("\nCipher Suite : ").append(con.getCipherSuite()).
                append("\n");

                X509Certificate[] certs = (X509Certificate[]) con.getServerCertificates();
                for (X509Certificate cert : certs) {
                    buff.append("\nCert SubjectDN: ").append(cert.getSubjectDN()).
                    append("\n\nCert Hash Code: ").append(cert.hashCode()).
                    append("\nCert Public Key Algorithm : ").append(cert.getPublicKey().getAlgorithm()).
                    append("\nCert Public Key Format : ").append(cert.getPublicKey().getFormat()).
                    append("\n");
                }
                System.out.println(buff);
            } catch (Exception e) {
                log.error(null, e);
            }

        }

    }

    private static void print_content(HttpsURLConnection con) {
        if (con != null) {

            try {

                System.out.println("****** Content of the URL ********");
                BufferedReader br =
                        new BufferedReader(
                        new InputStreamReader(con.getInputStream()));

                String input;

                while ((input = br.readLine()) != null) {
                    System.out.println(input);
                }
                br.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }
}
