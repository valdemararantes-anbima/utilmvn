package com.galgo.utils.cert_digital;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public class Cacerts {

	private File certsFile;
	private KeyStore ks;
	private char[] passphrase;
	private SSLSocketFactory socketFactory;
	private SavingTrustManager tm;

	public Cacerts(File certsFile, char[] passphrase) {
		this.certsFile = certsFile;
		this.passphrase = passphrase;
	}

	public Cacerts config() throws Exception {
		
		System.out.println("Usando arquivo " + certsFile.getCanonicalPath() + " como keystore");
		
		InputStream in = new FileInputStream(certsFile);
		KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());

		ks.load(in, passphrase);
		in.close();

		//SSLContext context = SSLContext.getInstance("TLS");
		SSLContext context = SSLContext.getInstance("SSL");
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory
				.getDefaultAlgorithm());
		tmf.init(ks);
		X509TrustManager defaultTrustManager = (X509TrustManager) tmf.getTrustManagers()[0];
		SavingTrustManager tm = new SavingTrustManager(defaultTrustManager);
		context.init(null, new TrustManager[] { tm }, null);
		socketFactory = context.getSocketFactory();
		this.tm = tm;
		this.ks = ks;
		return this;
	}

	public KeyStore getKs() {
		return ks;
	}

	public char[] getPassphrase() {
		return passphrase;
	}

	public SSLSocketFactory getSSLSocketFactory() {
		return socketFactory;
	}

	public SavingTrustManager getTrustManager() {
		return tm;
	}
}
