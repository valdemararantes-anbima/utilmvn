package com.galgo.utils.cert_digital;

import java.io.*;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Ao tentar acessar o servidor de WS de homologa��o, se o certificado n�o for encontrado
 * em cacerts, um outro Trust Store, jssecacerts, � criado ou alterado incluindo o
 * certificado do site.
 * 
 * @author valdemarneto
 *
 */
public class InstallCert {
	public static final String CACERTS = "cacerts";
	public static final String JSSECACERTS = "jssecacerts";
	public static final int TIMEOUT = 10000;
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("ddMMyyyy-hhmmss");

	private static final char[] HEXDIGITS = "0123456789abcdef".toCharArray();
	private static final Logger log = LoggerFactory.getLogger(InstallCert.class);
	private String host;
	private int port;

	public InstallCert(String host, int port) {
		this.host = host;
		this.port = port;
	}

	public static void main(String[] args) {
		//String host = "ws.producao.sistemagalgo";
        String host = "ws.homologacao.sistemagalgo";
		int port = 7843;
		if ((args.length == 1) || (args.length == 2)) {
			String[] c = args[0].split(":");
			host = c[0];
			port = (c.length == 1) ? 443 : Integer.parseInt(c[1]);
		}
		try {
			new InstallCert(host, port).install();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	private static String toHexString(byte[] bytes) {
		StringBuilder sb = new StringBuilder(bytes.length * 3);
		for (int b : bytes) {
			b &= 0xff;
			sb.append(HEXDIGITS[b >> 4]);
			sb.append(HEXDIGITS[b & 15]);
			sb.append(' ');
		}
		return sb.toString();
	}

	private static final void print(Object o) {
		System.out.println(o.toString());
	}

	private static final void print() {
		System.out.println();
	}
	
	public void install() throws Exception {
		File file = new File(JSSECACERTS);
		if (file.isFile() == false) {
			log.warn("Arquivo " + file.getCanonicalPath() + " nao encontrado");
			char SEP = File.separatorChar;
			File dir = new File(System.getProperty("java.home") + SEP + "lib" + SEP + "security");
			file = new File(dir, JSSECACERTS);

			if (file.isFile() == false) {
				log.warn("Arquivo " + JSSECACERTS + " nao encontrado na pasta " + dir.getCanonicalPath());
				log.warn("Utilizando o arquivo " + CACERTS + " da pasta " + dir.getCanonicalPath());
				file = new File(dir, CACERTS);
			} else {
				log.info("Arquivo " + file.getCanonicalPath() +
						" encontrado na pasta " + dir.getCanonicalPath());
			}
		} else {
			log.warn("Encontrado arquivo " + file.getCanonicalPath());
		}
		
		log.info("Carregando arquivo " + file + " ...");
		char[] passphrase = "changeit".toCharArray();
		Cacerts cacerts = new Cacerts(file, passphrase);
		cacerts.config();
		SSLSocketFactory factory = cacerts.getSSLSocketFactory();

		log.info("Tenta abrir uma conexao com " + host + ":" + port + " (timeout de " + TIMEOUT + "seg)...");
		SSLSocket socket = (SSLSocket) factory.createSocket(host, port);
		socket.setSoTimeout(TIMEOUT);
		try {
			log.info("Iniciando Handshake SSL...");
			socket.startHandshake();
			socket.close();
			print();
			log.info("Conexão aberta com sucesso. O certificado ja eh de confianca. Retornando...");
			return;
		} catch (SSLException e) {
			log.warn(e.getMessage(), e);
		}

		X509Certificate[] chain = cacerts.getTrustManager().chain;
		if (chain == null) {
			log.warn("Nao foi possivel obter a cadeia de certificados do servidor. Retornando...");
			return;
		}

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		print();
		log.info("O servidor enviou " + chain.length + " certificado(s):");
		MessageDigest sha1 = MessageDigest.getInstance("SHA1");
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		for (int i = 0; i < chain.length; i++) {
			X509Certificate cert = chain[i];
			print(" " + (i + 1) + " Subject " + cert.getSubjectDN());
			print("   Issuer  " + cert.getIssuerDN());
			sha1.update(cert.getEncoded());
			print("   sha1    " + toHexString(sha1.digest()));
			md5.update(cert.getEncoded());
			print("   md5     " + toHexString(md5.digest()));
			print();
		}

		System.out.println("Selecione o certificado a ser adicionado no keystore de certificados confiaveis ou 'q' para sair: [1]");
		String line = reader.readLine().trim();
		int k;
		try {
			k = (line.length() == 0) ? 0 : Integer.parseInt(line) - 1;
		} catch (NumberFormatException e) {
			log.info("KeyStore nao foi alterado. Retornando...");
			return;
		}

		X509Certificate cert = chain[k];
		String alias = host + "-" + (k + 1);
		KeyStore ks = cacerts.getKs();
		Certificate aliasCert = ks.getCertificate(alias);
		if (aliasCert != null) {
			log.info("O alias " + alias + " ja esta sendo utilizado no keystore");
			alias += "-" + DATE_FORMAT.format(new Date());
		}
		log.info("Inserindo o Certificado com o alias " + alias);
		ks.setCertificateEntry(alias, cert);

		OutputStream out = new FileOutputStream(JSSECACERTS);
		ks.store(out, passphrase);
		out.close();

		print();
		log.info(cert.toString());
		print();
		log.info("Certificado inserido no keystore " + JSSECACERTS + " usando alias '" + alias + "'");
	}
}
