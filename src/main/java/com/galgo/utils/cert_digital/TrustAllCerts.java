package com.galgo.utils.cert_digital;

import com.galgo.utils.ApplicationException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * As conexões SSL passam a confiar em qualquer certificado
 *
 * @author valdemar.arantes
 */
public class TrustAllCerts {

    private static final Logger log = LoggerFactory.getLogger(TrustAllCerts.class);

    private TrustAllCerts() {
    }

    public static void doIt() {

        log.info("Todos os certificados passarão a ser aceitos");

        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    log.debug("CERTIFICATE");
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    log.debug("CERTIFICATE");
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    log.debug("CERTIFICATE");
                }
            }
        };

        // Install the all-trusting trust manager
        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            SSLContext.setDefault(sc);
            SSLSocketFactory socketFactory = sc.getSocketFactory();
            HttpsURLConnection.setDefaultSSLSocketFactory(socketFactory);
        } catch (Exception e) {
            throw new ApplicationException(e);
        }


        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    }
}
