package com.galgo.utils;

public class ApplicationException extends RuntimeException {

	private static final long serialVersionUID = -8176610314078454351L;

	public ApplicationException() {
	}

	public ApplicationException(String message) {
		super(message);
	}

	public ApplicationException(Throwable cause) {
		super(cause);
	}

	public ApplicationException(String message, Throwable cause) {
		super(message, cause);
	}

}
