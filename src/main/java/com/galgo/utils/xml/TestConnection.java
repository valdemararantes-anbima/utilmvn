package com.galgo.utils.xml;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

/**
 * Testa uma conexao SSL
 * 
 * @author valdemarneto
 *
 */
public class TestConnection {
	private static final String GALGO_HOMOLOG = "ws.homologacao.sistemagalgo";
	private static final Integer GALGO_HOMOLG_PORT = 7843;
	//private static final int DNS_DEFAULT_PORT = 53;

	private String serverName;
	private int port;

	private SSLSocketFactory nonValidateSSLSocketFactory;

	public static void main(String[] args) {
		print("Executando...");

		//Proxy.init();
		//if (false)
		try {
			new TestConnection().start();
		} finally {
			print("Fim");
		}
	}

	private void start() {
		SSLContext sslContext;
		try {
			sslContext = SSLContext.getInstance("SSL");
			try {
				//				if (false)
				//				sslContext.init(null, new TrustManager[] {
				//					new X509TrustManager() {
				//						
				//						@Override
				//						public X509Certificate[] getAcceptedIssuers() {
				//							print("Anonymous TrustManager.getAcceptedIssuers()");
				//							return null;
				//						}
				//						
				//						@Override
				//						public void checkServerTrusted(X509Certificate[] chain, String authType)
				//								throws CertificateException {
				//							print("Anonymous TrustManager.checkServerTrusted()");
				//							
				//						}
				//						
				//						@Override
				//						public void checkClientTrusted(X509Certificate[] chain, String authType)
				//								throws CertificateException {
				//							print("Anonymous TrustManager.checkClientTrusted()");
				//						}
				//					}
				//				}, null);
				//				else {
				sslContext.init(null, null, null);
				//				}
			} catch (KeyManagementException e) {
				e.printStackTrace();
			}
			//SSLParameters sslParameters = sslContext.getDefaultSSLParameters();
			nonValidateSSLSocketFactory = sslContext.getSocketFactory();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		while (true) {
			readServerName();
			readServerPort();
			if (".".equals(serverName)) {
				System.exit(0);
			}
			connect();
		}
	}

	private void connect() {
		final int timeout = 10; // timeout para conectar em seg
		try {
			print("Conectando a " + serverName + ":" + port + " ( " + new Date() + " )");
			SocketAddress endpoint = new InetSocketAddress(serverName, port);
			SSLSocket socket = (SSLSocket) nonValidateSSLSocketFactory.createSocket();//(SSLSocket) SSLSocketFactory.getDefault().createSocket();
			socket.connect(endpoint, timeout * 1000);
			OutputStream out = socket.getOutputStream();
			out.write(1);
			print("Sucesso ( " + new Date() + " )");
			socket.close();
		} catch (UnknownHostException e) {
			print("Servidor " + serverName + " desconhecido");
		} catch (SocketTimeoutException e) {
			print("Esperei por " + timeout + " seg e nao consegui conectar ( " + new Date() + " )");
		} catch (SSLHandshakeException e) {
			print(new Date() + " - O certificado SSL nao pode ser validado:\n" + e.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String readPromptIns() {
		try {
			String s = null;
			while (true) {
				s = new BufferedReader(new InputStreamReader(System.in)).readLine();
				return s;
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return null;
	}

	private void readServerName() {
		print("\n\nDigite o nome do servidor (host default eh o " + GALGO_HOMOLOG + ") OU q para sair:");
		serverName = readPromptIns();
		if ("q".equalsIgnoreCase(serverName)) {
			print("FIM!!!");
			System.exit(0);
		} else if (serverName == null || "".equals(serverName.trim())) {
			print("Server Name is " + GALGO_HOMOLOG);
			serverName = GALGO_HOMOLOG;
		}
	}

	private void readServerPort() {
		print("Digite a porta (7843 eh a porta default):");
		String strPort = readPromptIns();
		print("port is " + GALGO_HOMOLG_PORT);
		port = (strPort + "").equals("") ? GALGO_HOMOLG_PORT : Integer.parseInt(strPort);
	}

	private static void print(Object o) {
		System.out.println(o.toString());
	}
}
