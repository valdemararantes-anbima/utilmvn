package com.galgo.utils.xml;

import com.galgo.utils.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
//import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

public class XMLUtils {

    private static final Logger log = LoggerFactory.getLogger(XMLUtils.class);
    private static final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

    static {
        factory.setNamespaceAware(true);
    }

    /**
     * Monta um DOM de XML a partir de Reader
     *
     * @param rdr
     * @return
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public static Document buildXml(Reader rdr) throws ParserConfigurationException, SAXException,
            IOException {
        char[] cbuf = new char[5000];
        int readChars = 0;

        StringBuilder strBuff = new StringBuilder();
        while ((readChars = rdr.read(cbuf)) != -1) {
            strBuff.append(cbuf, 0, readChars);
        }

        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource inStream = new InputSource();
        inStream.setCharacterStream(new StringReader(strBuff.toString()));
        return builder.parse(inStream);

    }

    /**
     * Monta um DOM de XML a partir de Reader
     *
     * @param xmlUrl
     * @return Objeto DOM do XML
     * @throws ApplicationException
     */
    public static Document buildXml(URL xmlUrl) throws ApplicationException {
        if (xmlUrl == null) {
            throw new ApplicationException("xmlUrl nulo");
        }

        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            log.error("Este erro encerra o aplicativo!", e);
            System.exit(-1);
            return null;
        }
        try {
            return builder.parse(xmlUrl.openStream());
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            throw new ApplicationException(e);
        }

    }

    public static String docToString(Document doc) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ElementToStream(doc.getDocumentElement(), baos);
        return new String(baos.toByteArray());
    }

    public static void dumpDocument(Document doc) throws TransformerException {
        dumpDocument(doc.getDocumentElement());
    }

    public static void dumpDocument(Node root) throws TransformerException {
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.transform(new DOMSource(root), new StreamResult(System.out));
    }

    public static void ElementToStream(Element element, OutputStream out) {
        try {
            DOMSource source = new DOMSource(element);
            StreamResult result = new StreamResult(out);
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();
            transformer.transform(source, result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Retorna o primeiro elemento de uma lista obtida atrav�s de
     * {@link Element}.getElementsByTagNameNS(...)
     *
     * @param elem
     * @param uri
     * @param tagName
     * @return
     * @throws TagNotFoundException
     */
    public static Node getNode(Element elem, String uri, String tagName)
            throws TagNotFoundException {
        NodeList elems = elem.getElementsByTagNameNS(uri, tagName);
        if (elems == null || elems.getLength() == 0) {
            throw new TagNotFoundException("Tag {" + uri + "}" + tagName + " nao foi encontrada");
        }

        return elems.item(0);
    }

    public static String getNodeValue(Element elem, String uri, String tagName)
            throws TagNotFoundException {
        NodeList elems = elem.getElementsByTagNameNS(uri, tagName);
        if (elems == null || elems.getLength() == 0) {
            throw new TagNotFoundException("Tag {" + uri + "}" + tagName + " nao foi encontrada");
        }

        return elems.item(0).getTextContent();
    }

    /**
     * @param elem
     * @param uri
     * @param tagName
     *
     * @return Retorna o valor do elemento elem ou nulo se nao conseguir encontrar
     */
    public static String getNodeValueDefault(Element elem, String uri, String tagName) {
        try {
            return getNodeValue(elem, uri, tagName);
        } catch (TagNotFoundException e) {
            return null;
        }
    }

    public static void main(String args[]) {
        String xml = "<raiz><filho><neto></neto></filho></raiz>";
        StringReader strReader = new StringReader(xml);
        try {
            Document doc = buildXml(strReader);
            doc.renameNode(doc.getElementsByTagName("filho").item(0), null, "filhoNovo");
            dumpDocument(doc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Realiza a serialização para XML do objeto obj
     *
     * @param obj Objeto a ser serializado
     * @return String XML do objeto serializado obj
     * @throws ApplicationException
     */
    public static String marshal(Object obj) {
        StringWriter sw = new StringWriter();
        try {
            log.trace("Serializando ...");
            JAXB.marshal(obj, sw);
            log.debug("Serialização concluída");
            return sw.toString();
        } catch (Exception e) {
            log.error(null, e);
            throw new ApplicationException(e);
        }
    }

    /**
     * Realiza a serialização para XML do objeto obj usando o Writer w como saída
     *
     * @param obj
     * @param w
     */
    public static void marshall(Object obj, Writer w) {
        JAXB.marshal(obj, w);
    }

    /**
     * Faz o marshalling e salva o objeto JAXB em arquivo utilizando encoding utf-8.
     *
     * @param obj
     * @param xmlFile
     */
    public static void marshallToFile(Object obj, File xmlFile) {
        try {
            if (!xmlFile.exists() && !xmlFile.createNewFile()) {
                throw new ApplicationException("Arquivo $xmlFile.canonicalFile não foi criado. Retornando sem salvar.");
            }
        } catch (IOException e) {
            throw new ApplicationException("Erro ao criar o arquivo " + xmlFile.getAbsolutePath(), e);
        }

        try {
            Writer writer = writer = new PrintWriter(xmlFile, StandardCharsets.UTF_8.displayName());
            JAXB.marshal(obj, writer);
            writer.close();
        } catch (Exception e) {
            throw new ApplicationException("Erro ao manipular o arquivo " + xmlFile.getAbsolutePath(), e);
        }
    }

    /**
     *
     * @param obj Objeto a ser serializado
     * @return Representacao DOM do objeto serializado obj
     */
    public static Document marshalToDOM(Object obj) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);

        try {
            Document doc = dbf.newDocumentBuilder().newDocument();
            getJAXBMarshaller(obj).marshal(obj, doc);
            return doc;
        } catch (Exception e) {
            log.error(null, e);
            throw new ApplicationException(e);
        }
    }

    public static Document renameNode(Document doc, String newName, String oldName) {
        Node firstChild = doc.getFirstChild();
        doc.renameNode(firstChild, newName, oldName);
        return doc;
    }

    /**
     * Gera uma string indentada do XML a partir da raiz
     *
     * @param doc
     * @return
     */
    public static final String toPrettyString(Document doc) {
        Node root = doc.getDocumentElement();
        return toPrettyString(root);
    }

    /**
     * Gera uma string indentada do XML a partir do noh root
     *
     * @param root
     * @return
     */
    public static final String toPrettyString(Node root) {
        Transformer transformer;
        try {
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            StringWriter wrt = new StringWriter();
            transformer.transform(new DOMSource(root), new StreamResult(wrt));
            return wrt.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     *
     * @param xml
     * @param cl
     * @return
     * @throws ApplicationException
     */
    @SuppressWarnings("unchecked")
    public static <T> T unmarshal(Node xml, Class<T> cl) throws ApplicationException {
        try {
            return (T) getJAXBUnmarshaller(cl).unmarshal(xml);
        } catch (JAXBException e) {
            log.error(null, e);
            throw new ApplicationException(e);
        }
    }

    /**
     *
     * @param xml
     * @param class1
     * @return
     * @throws ApplicationException
     */
    public static <T> T unmarshal(String xml, Class<T> class1) throws ApplicationException {
        Document doc;
        try {
            doc = buildXml(new StringReader(xml));
        } catch (Exception e) {
            log.error("Erro no parse do xml", e);
            throw new ApplicationException(e);
        }
        return unmarshal(doc, class1);
    }

    /**
     * Aplica uma transformação XSLT em um arquivo XML gerando um arquivo de saída
     *
     * @param xmlFilename XML a ser transformado
     * @param xslFilename XSL com o script de transformação
     * @param outFilename Arquivo de saída
     */
    public static void XSLTTransform(String xmlFilename, String xslFilename, String outFilename) {
        try {
            log.debug("Transformando o arquivo XML ...");
            String xmlSystemId = new File(xmlFilename).toURI().toURL().toExternalForm();
            String xslSystemId = new File(xslFilename).toURI().toURL().toExternalForm();
            String outSystemId = new File(outFilename).toURI().toURL().toExternalForm();
            StreamSource stylesource = new StreamSource(xslSystemId);
            Transformer transformer = TransformerFactory.newInstance().newTransformer(stylesource);
            transformer.transform(new StreamSource(xmlSystemId), new StreamResult(outSystemId));
            log.debug("Transformacao concluida");
        } catch (Exception e) {
            log.error(null, e);
            throw new ApplicationException();
        }

    }

    /**
     * @param ser Objeto a ser serializado
     * @return
     * @throws JAXBException
     * @throws PropertyException
     */
    private static Marshaller getJAXBMarshaller(Object ser) throws JAXBException,
            PropertyException {
        JAXBContext jc = JAXBContext.newInstance(ser.getClass());
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        return m;
    }

    /**
     *
     * @param cl
     * @return
     * @throws JAXBException
     */
    private static Unmarshaller getJAXBUnmarshaller(@SuppressWarnings("rawtypes") Class cl)
            throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(cl);
        return jc.createUnmarshaller();
    }
}
