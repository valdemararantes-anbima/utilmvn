/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galgo.utils;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * A utility class for converting objects between java.util.Date and XMLGregorianCalendar types
 * <p>
 * Ver <a href="http://java.dzone.com/articles/datatype-conversion-java">este site</a>
 */
public class XMLGregorianCalendarConversionUtil {

    // DatatypeFactory creates new javax.xml.datatype Objects that map XML
    // to/from Java Objects.
    private static DatatypeFactory df = null;

    static {
        try {
            df = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            throw new IllegalStateException("Error while trying to obtain a new instance of DatatypeFactory", e);
        }
    }

    /**
     * Converts an XMLGregorianCalendar to an instance of java.util.Date
     *
     * @param xmlGC
     * @return
     */
    public static java.util.Date asDate(XMLGregorianCalendar xmlGC) {
        if (xmlGC == null) {
            return null;
        } else {
            return xmlGC.toGregorianCalendar().getTime();
        }
    }

    /**
     * Converts a dateString into an instance of XMLGregorianCalendar as a dateTime
     *
     * @param stringDate
     * @param datePattern
     * @return
     * @throws ParseException
     */
    public static XMLGregorianCalendar asXMLGregorianCalendar(String stringDate, String datePattern)
        throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(datePattern);
        Date date = simpleDateFormat.parse(stringDate);
        return asXMLGregorianCalendar(date);
    }

    /**
     * Converts a java.util.Date into an instance of XMLGregorianCalendar
     *
     * @param date
     * @return
     */
    public static XMLGregorianCalendar asXMLGregorianCalendar(Date date) {
        if (date == null) {
            return null;
        } else {
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTimeInMillis(date.getTime());
            return df.newXMLGregorianCalendar(gc);
        }
    }

    /**
     * Converts a java.util.Date into an instance of XMLGregorianCalendar Date
     *
     * @param date
     * @return
     */
    public static XMLGregorianCalendar asXMLGregorianCalendar_Date(Date date) {
        XMLGregorianCalendar xmlGregCal = asXMLGregorianCalendar(date);
        xmlGregCal.setHour(DatatypeConstants.FIELD_UNDEFINED);
        xmlGregCal.setMinute(DatatypeConstants.FIELD_UNDEFINED);
        xmlGregCal.setSecond(DatatypeConstants.FIELD_UNDEFINED);
        xmlGregCal.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
        return xmlGregCal;
    }

    /**
     * Converts a dateString into an instance of XMLGregorianCalendar as a date, without time
     *
     * @param stringDate
     * @param datePattern
     * @return
     * @throws ParseException
     */
    public static XMLGregorianCalendar asXMLGregorianCalendar_Date(String stringDate, String datePattern)
        throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(datePattern);
        Date date = simpleDateFormat.parse(stringDate);
        return asXMLGregorianCalendar_Date(date);
    }

    /**
     * Converts a java.time.LocalDate into an instance of XMLGregorianCalendar Date
     *
     * @param date
     * @return XMLGregorianCalendar
     */
    public static XMLGregorianCalendar asXMLGregorianCalendar_Date(LocalDate date) {
        final GregorianCalendar gc = GregorianCalendar.from(date.atStartOfDay(ZoneId.systemDefault()));
        final XMLGregorianCalendar xmlGregCal = df.newXMLGregorianCalendar(gc);
        xmlGregCal.setHour(DatatypeConstants.FIELD_UNDEFINED);
        xmlGregCal.setMinute(DatatypeConstants.FIELD_UNDEFINED);
        xmlGregCal.setSecond(DatatypeConstants.FIELD_UNDEFINED);
        xmlGregCal.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
        return xmlGregCal;
    }

    /**
     * Converts a java.time.LocalDateTime into an instance of XMLGregorianCalendar
     *
     * @param date
     * @return XMLGregorianCalendar
     */
    public static XMLGregorianCalendar asXMLGregorianCalendar(LocalDateTime date) {
        final GregorianCalendar gc = GregorianCalendar.from(date.atZone(ZoneId.systemDefault()));
        final XMLGregorianCalendar xmlGregCal = df.newXMLGregorianCalendar(gc);
        return xmlGregCal;
    }

    public static XMLGregorianCalendar asXMLGregorianCalendar_Hour(String stringDate, String datePattern)
        throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(datePattern);
        Date date = simpleDateFormat.parse(stringDate);
        return asXMLGregorianCalendar_Hour(date);
    }

    public static void main(String[] args) {
        Date currentDate = new Date(); // Current date

        // java.util.Date to XMLGregorianCalendar
        XMLGregorianCalendar xmlGC = XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar(currentDate);
        System.out.println("Current date in XMLGregorianCalendar format: " + xmlGC.toString());

        // XMLGregorianCalendar to java.util.Date
        System.out.println("Current date in java.util.Date format: " + XMLGregorianCalendarConversionUtil.asDate(xmlGC)
                .toString());

        System.out.println("Teste para LocaDateTime: " + asXMLGregorianCalendar(LocalDateTime.now()));
    }

    private static XMLGregorianCalendar asXMLGregorianCalendar_Hour(Date date) {
        XMLGregorianCalendar xmlGregCal = asXMLGregorianCalendar(date);
        xmlGregCal.setYear(DatatypeConstants.FIELD_UNDEFINED);
        xmlGregCal.setMonth(DatatypeConstants.FIELD_UNDEFINED);
        xmlGregCal.setDay(DatatypeConstants.FIELD_UNDEFINED);
        return xmlGregCal;
    }
}
