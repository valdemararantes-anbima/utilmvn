/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galgo.utils;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.beanutils.Converter;

/**
 *
 * @author valdemar.arantes
 */
public class BeanUtils extends org.apache.commons.beanutils.BeanUtils {

    static {
        registerXMLGregorianCalendarConverter();
    }

    /**
     * Atribui o valor value à propriedade name do objeto bean instanciando as propriedades
     * intermediárias se necessário.
     *
     * @param bean
     * @param name
     * @param value
     * <p>
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public static void setProperty(Object bean, String name, Object value) throws
            IllegalAccessException, InvocationTargetException {
        PropertyUtils.instantiateNestedProperties(bean, name);
        beanUtilsBean.setProperty(bean, name, value);
    }

    private static void registerXMLGregorianCalendarConverter() {
        ConvertUtils.register(new Converter() {

            @Override
            public <T> T convert(Class<T> type, Object value) {
                if (String.class.isAssignableFrom(value.getClass())) {
                    String dtStr = (String) value;
                    try {
                        return (T) XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar_Date(
                                dtStr, "dd-MM-yyyy");
                    } catch (ParseException e) {
                        try {
                            return (T) XMLGregorianCalendarConversionUtil.
                                    asXMLGregorianCalendar_Date(
                                            dtStr, "dd/MM/yyyy");
                        } catch (ParseException e1) {
                            String err = String.format(
                                    "A data %1s não está em um dos formatos %2s ou %3s", value,
                                    "dd-MM-yyyy", "dd/MM/yyyy");
                            throw new RuntimeException(err);
                        }
                    }
                } else {
                    throw new RuntimeException(String.format("Argumento %1s é do tipo %2s quando "
                            + "deveria ser do tipo String", value, value.getClass()));
                }
            }
        }, XMLGregorianCalendar.class);
    }

    private static class EnumAwareConvertUtilsBean extends ConvertUtilsBean {

        private final EnumConverter enumConverter = new EnumConverter();

        public Converter lookup(Class clazz) {
            final Converter converter = super.lookup(clazz);
        // no specific converter for this class, so it's neither a String, (which has a default converter),
            // nor any known object that has a custom converter for it. It might be an enum !
            if (converter == null && clazz.isEnum()) {
                return enumConverter;
            } else {
                return converter;
            }
        }

        private class EnumConverter implements Converter {

            public Object convert(Class type, Object value) {
                return Enum.valueOf(type, (String) value);
            }
        }
    }

    private static final BeanUtilsBean beanUtilsBean = new BeanUtilsBean(new EnumAwareConvertUtilsBean());
}
