/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galgo.utils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Map;
import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.beanutils.expression.DefaultResolver;
import org.apache.commons.beanutils.expression.Resolver;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
public class NestedPropertySetter extends PropertyUtilsBean {

    private static final Logger log = LoggerFactory.getLogger(NestedPropertySetter.class);
    private Resolver resolver = new DefaultResolver();

//    public static final void main(String... args) {
//        try {
//            log.info("******************************");
//
//            BalForSubAcctBrData bean = new BalForSubAcctBrData();
//
//            new NestedPropertySetter().setProperty(bean, "riskLv.cd", "aaa");
//            log.debug("bean={}", ToStringBuilder.reflectionToString(bean));
//
//            log.info("FIM **************************");
//        } catch (IllegalAccessException | InvocationTargetException |
//                NoSuchMethodException e) {
//            log.error("Exceções do PropertyUtils", e);
//        } catch (Exception e) {
//            log.error("Outras exceções", e);
//        }
//    }

    public void setNestedProperty(Object bean,
            String name, Object value)
            throws IllegalAccessException, InvocationTargetException,
            NoSuchMethodException {

        if (bean == null) {
            throw new IllegalArgumentException("No bean specified");
        }
        if (name == null) {
            throw new IllegalArgumentException("No name specified for bean class '" + bean.
                    getClass() + "'");
        }

        String[] propNameList = StringUtils.split(name, ".");
        propNameList = Arrays.copyOf(propNameList, propNameList.length - 1);
        Object lastBean = bean;
        for (String propName : propNameList) {
            Object lastBeanCandidate = getProperty(lastBean, propName);
            if (lastBeanCandidate == null) {
                try {
                    PropertyDescriptor descr = getPropertyDescriptor(lastBean, propName);
                    Object newInstance = descr.getPropertyType().newInstance();
                    setSimpleProperty(lastBean, propName, newInstance);
                } catch (InstantiationException ex) {
                    log.error(null, ex);
                }
            }
        }

        // Resolve nested references
        while (resolver.hasNested(name)) {
            String next = resolver.next(name);
            Object nestedBean = null;
            if (bean instanceof Map) {
                nestedBean = getPropertyOfMapBean((Map<?, ?>) bean, next);
            } else if (resolver.isMapped(next)) {
                nestedBean = getMappedProperty(bean, next);
            } else if (resolver.isIndexed(next)) {
                nestedBean = getIndexedProperty(bean, next);
            } else {
                nestedBean = getSimpleProperty(bean, next);
            }
            if (nestedBean == null) {
                throw new NestedNullException("Null property value for '" + name
                        + "' on bean class '" + bean.getClass() + "'");
            }
            bean = nestedBean;
            name = resolver.remove(name);
        }

        if (resolver.isMapped(name)) {
            setMappedProperty(bean, name, value);
        } else if (resolver.isIndexed(name)) {
            setIndexedProperty(bean, name, value);
        } else {
            setSimpleProperty(bean, name, value);
        }

    }

}
