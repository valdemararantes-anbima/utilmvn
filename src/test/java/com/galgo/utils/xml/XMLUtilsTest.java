package com.galgo.utils.xml;

import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;

import java.io.File;

/**
 * Created by Valdemar on 07/06/2016.
 */
public class XMLUtilsTest {

    @Test
    public void testMarshallToFile() throws Exception {
        Document document = XMLUtils.buildXml(Thread.currentThread().getContextClassLoader().getResource("escola.xml"));
        File testFile = new File("test_file.xml");
        XMLUtils.marshallToFile(document, testFile);
        Assert.assertTrue(testFile.exists() && testFile.length() > 0);
        testFile.delete();
    }
}