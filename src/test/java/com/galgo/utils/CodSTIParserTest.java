package com.galgo.utils;

import com.google.common.collect.Lists;
import org.junit.Test;

import java.text.ParseException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by valdemar.arantes on 16/03/2016.
 */
public class CodSTIParserTest {
    @Test
    public void testParse() {
        try {
            assertEquals(123456, CodSTIParser.parse("123456").get(0).intValue());
            assertEquals(123456, CodSTIParser.parse(" 12345-6 ").get(0).intValue());
            assertEquals(123456, CodSTIParser.parse(" -12345-6 ").get(0).intValue());
            assertEquals(123456, CodSTIParser.parse(" -123-45-6 ").get(0).intValue());
            assertNotEquals(123456, CodSTIParser.parse(" -123-45- 6 ").get(0).intValue());

            List<Integer> expectedList = Lists.newArrayList(111, 222);
            assertTrue(expectedList.containsAll(CodSTIParser.parse("111 222")));
            assertTrue(expectedList.containsAll(CodSTIParser.parse("\"111 222\"")));
            assertTrue(expectedList.containsAll(CodSTIParser.parse("111,\n222")));
        } catch (ParseException e) {
            e.printStackTrace();
            fail(e.toString());
        }
    }

    @Test
    public void testParseList() {
        try {
            List<Integer> expectedList = Lists.newArrayList(111, 222);
            assertTrue(expectedList.containsAll(CodSTIParser.parse("111 222")));
            assertTrue(expectedList.containsAll(CodSTIParser.parse("\"111 222\"")));
            assertTrue(expectedList.containsAll(CodSTIParser.parse("111,\n222")));
        } catch (ParseException e) {
            e.printStackTrace();
            fail(e.toString());
        }
    }
}