
import com.galgo.utils.Cronometro;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author valdemar.arantes
 */
public class CronometroTest {

    private static final Logger log = LoggerFactory.getLogger(CronometroTest.class);

    @Test
    public void test() {
        Cronometro cron = Cronometro.start();
        try {
            log.debug("Dormingo 2s ...");
            Thread.sleep(2000);

            log.debug("Pause");
            log.debug(cron.pause().toString());

            log.debug("Dormindo 1s ...");
            Thread.sleep(1000);
            log.debug(cron.toString());

            log.debug("Resume...");
            cron.resume();
            log.debug("Dormindo 1s ...");
            Thread.sleep(1000);
            log.debug(cron.toString());

            log.debug("Dormindo 1s ...");
            Thread.sleep(1000);
            log.debug(cron.toString());

            log.debug("End");
            cron.end();
            log.debug(cron.toString());

        } catch (Exception e) {
            log.error(null, e);
        }

    }

}
